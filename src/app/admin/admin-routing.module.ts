import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuard } from '../guard/auth-guard.guard';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: DashboardComponent
  },
  {
    path:'logout',
    canActivate: [AuthGuard],
    component: LogoutComponent
  },
  {
    path:'projects',
    loadChildren:'app/admin/project/project.module#ProjectModule'
  },
  {
    path: 'users',
    loadChildren: 'app/admin/user/user.module#UserModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthService } from '../services/auth.service';
import { ToastrService } from '../services/toastr.service';
import { AuthGuard } from '../guard/auth-guard.guard';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  declarations: [DashboardComponent, LogoutComponent],
  providers: [AuthService, ToastrService, AuthGuard]
})
export class AdminModule { }

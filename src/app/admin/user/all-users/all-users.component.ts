import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { IUser } from '../../../models/user';
@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {


  users: IUser[]=[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getallUsers();
  }
  getallUsers() {
    this.userService.getallUsers()
      .subscribe(users => {
        this.users = users as IUser[];
        console.log(this.users.length);
      });
  }
}

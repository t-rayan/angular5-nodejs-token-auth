import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { LoginComponent } from './login/login.component';

import { AuthService } from '../services/auth.service';
import { ToastrService } from '../services/toastr.service';
import { AuthGuard } from '../guard/auth-guard.guard';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    UserManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LoginComponent],
  providers: [AuthService, ToastrService, AuthGuard]
})
export class UserManagementModule { }
